<?php

/**
 * @file
 * Contains theme functions for HFC Global module.
 *
 * @see hfcc_global.module
 */

/**
 * Theme preprocess function for theme_hfcc_global_pseudo_field() and hfcc_global_pseudo_field.tpl.php.
 *
 * @see template_preprocess_field()
 */
function template_preprocess_hfcc_global_pseudo_field(&$variables) {
  $element = $variables['element'];

  // Provide a default value for #label_display.

  if (empty($element['#label_display'])) {
    $element['#label_display'] = 'above';
  }

  // There's some overhead in calling check_plain() so only call it if the label
  // variable is being displayed. Otherwise, set it to NULL to avoid PHP
  // warnings if a theme implementation accesses the variable even when it's
  // supposed to be hidden. If a theme implementation needs to print a hidden
  // label, it needs to supply a preprocess function that sets it to the
  // sanitized element title or whatever else is wanted in its place.

  $variables['label_hidden'] = ($element['#label_display'] == 'hidden');
  $variables['label'] = ($variables['label_hidden'] || empty($element['#label'])) ? NULL : check_plain($element['#label']);

  // We want other preprocess functions and the theme implementation to have
  // fast access to the field item render arrays. The item render array keys
  // (deltas) should always be a subset of the keys in #items, and looping on
  // those keys is faster than calling element_children() or looping on all keys
  // within $element, since that requires traversal of all element properties.
  $variables['items'] = array();
  if (!empty($element['#markup'])) {
    $variables['items'][] = array('#markup' => $element['#markup']);
  }
  elseif (!empty($element['#items'])) {
    foreach ($element['#items'] as $delta => $item) {
      // if (!empty($item)) {
        $variables['items'][$delta] = $item;
      // }
    }
  }

  // Add default CSS classes. Since there can be many fields rendered on a page,
  // save some overhead by calling strtr() directly instead of
  // drupal_html_class().
  $variables['field_name_css'] = strtr($element['#field_name'], '_', '-');
  $variables['classes_array'] = array(
    'field',
    'field-name-' . $variables['field_name_css'],
    'field-label-' . $element['#label_display'],
    'clearfix'
  );

  // Add specific suggestions that can override the default implementation.
  $variables['theme_hook_suggestions'] = array(
    'hfcc_global_pseudo_field__' . $element['#field_name'],
  );
}
