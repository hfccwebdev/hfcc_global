<?php

/**
 * @file
 * Contains drush commands provided by this module.
 */

/**
 * Implements hook_drush_command().
 */
function hfcc_global_drush_command() {
  return [
    'hfcc-global-content-reassign' => [
      'description' => 'Reassign content to a new owner.',
      'aliases' => ['chown'],
      'arguments' => [
        'oldname' => 'Name of original content owner',
        'newname' => 'Name of new content owner',
      ],
      'options' => [
        'comments' => 'Include comments in ownership changes',
      ],
      'examples' => [
        'drush chown smeagol gollum --comments' => 'Will reassign all content and comments authored by user smeagol to user gollum',
      ],
    ],
  ];
}

/**
 * Callback for the drush content-reassign command.
 */
function drush_hfcc_global_content_reassign() {
  $names = _convert_csv_to_array(func_get_args());
  if(count($names) !== 2) {
    return drush_set_error(dt('Please specify both the original and new username.'));
  }
  list($oldname, $newname) = $names;
  $comments = drush_get_option('comments');
  HfcGlobalReassignContent::create()->update($oldname, $newname, $comments);
}
