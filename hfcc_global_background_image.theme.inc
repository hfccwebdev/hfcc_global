<?php

/**
 * @file
 * Formatter for hfcc_global_background_image.
 */
function theme_hfcglobal_background_image($variables) {
  $item = $variables['element']['#item'];

  if (!empty($variables['element']['#image_style'])) {
    $path = image_style_url($variables['element']['#image_style'], $item['uri']);
  }
  else {
    $path = file_create_url($item['uri']);
  }

  $inline_style = "background-image: url($path)";
  return "<div class=\"hfcglobal-background-image\" style=\"$inline_style\"></div>";
}
