<?php

/**
 * Provides a base class for content APIs.
 */
abstract class HfcGlobalBaseContentApi {

  /**
   * Returns the user index.
   */
  public static function users(): void {

    self::checkApiKey();

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("
      SELECT u.uid, u.name, u.mail, u.status FROM {users} u
      JOIN {node} n ON n.uid = u.uid
      WHERE u.uid > 0 AND u.status = 1
      GROUP BY u.uid ORDER BY u.uid
    ")->fetchAllAssoc('uid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * File index.
   */
  public static function fileIndex(): void {

    self::checkApiKey();

    // Disable caching for this callback.
    $GLOBALS['conf']['cache'] = 0;
    $result = db_query("
      SELECT fid, uri, uuid, `timestamp` FROM {file_managed}
      ORDER BY uri
    ")->fetchAllAssoc('fid');
    drupal_json_output($result);
    drupal_exit();
  }

  /**
   * Returns a file object.
   */
  public static function file(object $file): void {

    self::checkApiKey();

    if (file_exists($file->uri)) {
      $file->url = file_create_url($file->uri, ['absolute' => TRUE]);
      $file->sha1 = sha1_file($file->uri);
      drupal_json_output($file);
      drupal_exit();
    }
    drupal_not_found();
    drupal_exit();
  }

  /**
   * Check the api key.
   */
  protected static function checkApiKey(): void {

    $authorization = getallheaders()['Authorization'] ?? NULL;
    if (empty($authorization)) {
      drupal_access_denied();
      drupal_exit();
    }
    $authorization = preg_replace('/^Bearer /', '', $authorization);
    if ($authorization !== variable_get('api-secret')) {
      drupal_access_denied();
      drupal_exit();
    }
  }

  /**
   * Convert file URIs.
   */
  protected static function convertFileUri(&$field): void {
    foreach ($field[LANGUAGE_NONE] as $key => $file) {
      $url = file_create_url($file['uri'], ['absolute' => TRUE]);
      $field[LANGUAGE_NONE][$key]['uri'] = $url;
    }
  }

  /**
   * Load child paragraph entities and convert file URIs.
   */
  protected static function loadParagraphs(&$paragraphs): void {
    foreach ($paragraphs[LANGUAGE_NONE] as $key => $metadata) {

      $paragraph = paragraphs_item_revision_load($metadata['revision_id']);

      if (!empty($paragraph->field_para_background_photo)) {
        self::convertFileUri($paragraph->field_para_background_photo);
      }

      if (!empty($paragraph->field_para_caption_photos)) {
        self::convertFileUri($paragraph->field_para_caption_photos);
      }

      if (!empty($paragraph->field_para_photos)) {
        self::convertFileUri($paragraph->field_para_photos);
      }

      if (!empty($paragraph->field_person_photo)) {
        self::convertFileUri($paragraph->field_person_photo);
      }

      if (!empty($paragraph->field_paragraphs)) {
        self::loadParagraphs($paragraph->field_paragraphs);
      }

      if (!empty($paragraph->field_para_nested)) {
        self::loadParagraphs($paragraph->field_para_nested);
      }

      $paragraphs[LANGUAGE_NONE][$key] = $paragraph;
    }
  }

}
