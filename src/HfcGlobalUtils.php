<?php

/**
 * Contains the HFC Global Utilities class.
 */
class HfcGlobalUtils implements HfcGlobalUtilsInterface {

  /**
   * {@inheritdoc}
   */
  public function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public static function formatOfficePhone($value, $allow_extension = FALSE) {

    // Start by completely stripping all but digits.
    $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    $value = str_replace('-', '', $value);

    switch (strlen($value)) {
      case '4':
        $value = $allow_extension ? $value : NULL;
        break;

      case '7':
        $value = preg_replace('/(\d{3})(\d{4})/', '313-$1-$2', $value);
        break;

      case '10':
        $value = preg_replace('/(\d{3})(\d{3})(\d{4})/', '$1-$2-$3', $value);
        break;

      default:
        // If we cannot parse value, show raw value
        // only if show_extension setting is enabled.
        $value = $allow_extension ? $value : NULL;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function findOrphanedFields($remove = FALSE) {
    $instances = field_info_instances();
    $bundles = field_info_bundles();

    system_rebuild_module_data();

    foreach ($instances as $entity_type => $type_bundles) {
      foreach ($type_bundles as $bundle => $bundle_instances) {
        foreach ($bundle_instances as $field_name => $instance) {
          if (!isset($bundles[$entity_type])) {
            drupal_set_message(t('Field @field_name entity type @entity_type not found.',
              ['@field_name' => $field_name, '@entity' => $entity_type]
            ), 'error');
          }
          elseif (!isset($bundles[$entity_type][$bundle])) {
            $values = [
              '@field_name' => $field_name,
              '@entity' => $entity_type,
              '@bundle' => $bundle,
            ];
            drupal_set_message(t('Field @field_name bundle @entity_type @bundle not found.', $values), 'error');
            if ($remove) {
              drupal_set_message(t('Removing field instance @field_name @entity_type @bundle.', $values), 'warning');
              field_delete_instance($instance);
            }
          }
        }
      }
    }
  }

}
