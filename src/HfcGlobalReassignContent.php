<?php

/**
 * Contains a Service to reassign content ownership from one user to another.
 */
class HfcGlobalReassignContent {

  /**
   * Creates an instance of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Mass updates nodes from one owner ID to another.
   *
   * @param string $old_name
   *   The name of the current owner.
   * @param int $new_name
   *   The name of the new owner.
   * @param bool $reassign_comments
   *   Flag to reassign comments. Defaults to FALSE.
   *
   * @see node_user_cancel()
   * @see comment_user_cancel()
   */
  public function update($old_name, $new_name, $reassign_comments = FALSE) {

    if (!$this->checkCurrentUser()) return;

    $old_uid = user_load_by_name($old_name)->uid ?: $this->userFailMessage($old_name);
    $new_uid = user_load_by_name($new_name)->uid ?: $this->userFailMessage($new_name);

    if (!$old_uid || !$new_uid) {
      drupal_set_message(t('Cannot update content.'), 'error');
      return;
    }

    if ($reassign_comments) {
      $this->updateCommentOwner($old_uid, $new_uid);
    }

    if ($nids = $this->fetchUserNids($old_uid)) {

      drupal_set_message(t("Updating owner of @nids from @oldname (@olduid) to @newname (@newuid)", [
        '@nids' => format_plural(count($nids), '1 node', '@count nodes'),
        '@oldname' => $old_name,
        '@olduid' => $old_uid,
        '@newname' => $new_name,
        '@newuid' => $new_uid,
      ]));

      // I tried using node_mass_update() here, but it didn't work.
      // We'll run this from drush so we don't really need batch anyway.

      $updates = [
        'uid' => $new_uid,
        'revision' => 1,
        'log' => t('Bulk update content assignment from @old to @new.', ['@old' => $old_name, '@new' => $new_name]),
      ];

      // @see _node_mass_update_helper()
      foreach ($nids as $nid) {
        $node = node_load($nid, NULL, TRUE);
        // For efficiency manually save the original node before applying any changes.
        $node->original = clone $node;
        foreach ($updates as $property => $value) {
          $node->$property = $value;
        }
        node_save($node);
      }
    }
    else {
      drupal_set_message(t('No matching content found for user @user', ['@user' => $old_name]), 'warning');
    }
  }

  /**
   * Check current user is not Anonymous.
   */
  private function checkCurrentUser() {
    global $user;
    if ($user->uid == 0) {
      drupal_set_message(t('This process cannot be run by the Anonymous user!'), 'error');
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Set a mesage if the user could not be loaded.
   *
   * @param string $name
   *   The user name that could not be loaded.
   * @return bool
   *   Always return FALSE
   */
  private function userFailMessage($name) {
    drupal_set_message(t('Could not load user %name', ['%name' => $name]), 'error');
    return FALSE;
  }

  /**
   * Update comments for the given user id.
   *
   * @param int $old_uid
   *   The uid of the current owner.
   * @param int $new_uid
   *   The uid of the new owner.
   *
   * @see comment_user_cancel()
   */
  private function updateCommentOwner($old_uid, $new_uid) {
      $comments = comment_load_multiple([], ['uid' => $old_uid]);
      drupal_set_message(t("Found @comments to update.", [
        '@comments' => format_plural(count($comments), '@count comment', '@count comments')
      ]));
      foreach ($comments as $comment) {
        $comment->uid = $new_uid;
        comment_save($comment);
      }
  }

  /**
   * Query nodes for current owner.
   *
   * @param int $uid
   *   User ID to query.
   *
   * @return int[]
   *   An array of corresponding nids.
   */
  private function fetchUserNids($uid) {
    return db_select('node', 'n')
      ->fields('n', ['nid'])
      ->condition('uid', $uid)
      ->execute()
      ->fetchCol();
  }
}
