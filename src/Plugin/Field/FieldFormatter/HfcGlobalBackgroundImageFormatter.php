<?php

/**
 * Defines the HfcGlobalBackgroundImageFormatter class.
 */
class HfcGlobalBackgroundImageFormatter {

  /**
   * Contains the values for hook_field_formatter_info().
   *
   * @see image_field_formatter_info()
   */
  public static function info() {
    return [
      'label' => t('Background Image'),
      'field types' => ['image'],
      'settings' => ['image_style' => ''],
    ];
  }

  /**
   * Contains the values for hook_field_formatter_settings_form().
   *
   * @see image_field_formatter_settings_form()
   */
  public static function settingsForm($field, $instance, $view_mode, $form, &$form_state) {
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];

    $image_styles = image_style_options(FALSE, PASS_THROUGH);
    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $settings['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );
    return $element;
  }

  /**
   * Contains the values for hook_field_formatter_settings_summary().
   *
   * @see image_field_formatter_settings_summary()
   */
  public static function settingsSummary($field, $instance, $view_mode) {
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];

    $summary = [];

    $image_styles = image_style_options(FALSE, PASS_THROUGH);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$settings['image_style']])) {
      $summary[] = t('Image style: @style', ['@style' => $image_styles[$settings['image_style']]]);
    }
    else {
      $summary[] = t('Original image');
    }

    return implode('<br />', $summary);
  }

  /**
   * Contains the code for hook_field_formatter_view().
   *
   * Build a renderable array for a field value.
   *
   * @param $entity_type
   *   The type of $entity.
   * @param $entity
   *   The entity being displayed.
   * @param $field
   *   The field structure.
   * @param $instance
   *   The field instance.
   * @param $langcode
   *   The language associated with $items.
   * @param $items
   *   Array of values for this field.
   * @param $display
   *   The display settings to use, as found in the 'display' entry of instance
   *   definitions. The array notably contains the following keys and values;
   *   - type: The name of the formatter to use.
   *   - settings: The array of formatter settings.
   *
   * @return
   *   A renderable array for the $items, as an array of child elements keyed
   *   by numeric indexes starting from 0.
   *
   * @see image_field_formatter_view()
   */
  public static function view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

    $element = [];

    foreach ($items as $delta => $item) {
      if (isset($link_file)) {
        $uri = [
          'path' => file_create_url($item['uri']),
          'options' => [],
        ];
      }
      $element[$delta] = [
        '#theme' => 'hfcglobal_background_image',
        '#item' => $item,
        '#image_style' => $display['settings']['image_style'],
      ];
    }
    return $element;
  }
}
