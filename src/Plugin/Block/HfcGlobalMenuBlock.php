<?php

/**
 * Defines the global menu block.
 */
class HfcGlobalMenuBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info($delta = NULL) {

    $info = [
      'hfcc_global_quick_links' => 'HFC Global: Quick Links menu.',
      'hfcc_global_program_links' => 'HFC Global: Programs and Courses Links menu.',
      'hfcc_global_services_links' => 'HFC Global: Student Services Links menu.',
      'hfcc_global_life_links' => 'HFC Global: Student Life Links menu.',
      'hfcc_global_fatbutton_links' => 'HFC Global: Fat Button Links menu.',
      'hfcc_global_footer_links' => 'HFC Global: Footer Links menu.',
      'hfcc_global_audience_links' => 'HFC Global: Audience Links menu.',
    ];

    return [
      'info' => t($info[$delta]),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label($delta = NULL) {

    $labels = [
      'hfcc_global_quick_links' => 'Quick Links',
      'hfcc_global_program_links' => 'Programs & Courses',
      'hfcc_global_services_links' => 'Student Services',
      'hfcc_global_life_links' => 'Student Life',
      'hfcc_global_fatbutton_links' => NULL,
      'hfcc_global_footer_links' => NULL,
      'hfcc_global_audience_links' => NULL,
    ];
    return t($labels[$delta]);
  }

  /**
   * {@inheritdoc}
   */
  public function view($delta = NULL) {
    $output = [];
    $this->build($output, $delta);

    if (!empty($output)) {
      return ['subject' => $this->label($delta), 'content' => $output];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output, $delta = '') {

    $items = [];
    switch ($delta) {
      case 'hfcc_global_quick_links':
        $class = 'quick-links';
        $items[] = $this->menuItem(t('HFC Portal'), 'https://my.hfcc.edu/', 'myhfcc');
        $items[] = $this->menuItem(t('WebAdvisor'), 'https://my.hfcc.edu/WebAdvisor/WebAdvisor');
        $items[] = $this->menuItem(t('Password Help'), 'https://www.hfcc.edu/password');
        $items[] = $this->menuItem(t('Student Email'), 'https://www.hfcc.edu/hawkmail', 'hawkmail' , t('HawkMail'));
        $items[] = $this->menuItem(t('Faculty/Staff Email'), 'https://gwweb.hfcc.edu/gw/webacc', 'groupwise', t('GroupWise'));
        $items[] = $this->menuItem(t('Online Learning'), 'https://www.hfcc.edu/online-learning');
        $items[] = $this->menuItem(t('HFC Online'), 'https://online.hfcc.edu/');
        $items[] = $this->menuItem(t('Transcripts'), 'https://www.hfcc.edu/registration-and-records/transcripts');
        $items[] = $this->menuItem(t('Campus Map'), 'https://www.hfcc.edu/map');
        $items[] = $this->menuItem(t('Directory'), 'https://www.hfcc.edu/directory');
        $items[] = $this->menuItem(t('Catalog'), 'https://catalog.hfcc.edu/');
        break;
      case 'hfcc_global_program_links':
        $class = 'program-links';
        $items[] = $this->menuItem(t('Catalog'), 'https://catalog.hfcc.edu/', 'catalog');
        $items[] = $this->menuItem(t('Programs of Study'), 'https://catalog.hfcc.edu/programs', 'programs');
        $items[] = $this->menuItem(t('Course Descriptions'), 'https://catalog.hfcc.edu/courses', 'courses');
        $items[] = $this->menuItem(t('Academic Calendar'), 'https://www.hfcc.edu/academic-calendar');
        $items[] = $this->menuItem(t('Search for Classes'), 'https://sss.hfcc.edu/Student/Courses');
        $items[] = $this->menuItem(t('English Language Institute ELI'), 'https://www.hfcc.edu/academics/programs/english-language-institute');
        $items[] = $this->menuItem(t('Skilled Trades Training'), 'https://www.hfcc.edu/trade-and-apprentice');
        $items[] = $this->menuItem(t('M-TEC'), 'https://mtec.hfcc.edu', 'mtec');
        $items[] = $this->menuItem(t('University Center'), 'https://www.hfcc.edu/university-center');
        break;
      case 'hfcc_global_services_links':
        $class = 'services-links';
        $items[] = $this->menuItem(t('Welcome Center'), 'https://www.hfcc.edu/student-services/welcome-center');
        $items[] = $this->menuItem(t('Registration & Records'), 'https://www.hfcc.edu/registration-and-records');
        $items[] = $this->menuItem(t('Financial Aid'), 'https://www.hfcc.edu/financial-aid');
        $items[] = $this->menuItem(t('Bookstore'), 'https://collegestore.hfcc.edu/');
        $items[] = $this->menuItem(t('Library'), 'https://library.hfcc.edu/');
        $items[] = $this->menuItem(t('Learning Lab & Tutoring'), 'https://learnlab.hfcc.edu/');
        $items[] = $this->menuItem(t('International Admissions'), 'https://www.hfcc.edu/international');
        $items[] = $this->menuItem(t('Veterans Services'), 'https://www.hfcc.edu/veterans');
        $items[] = $this->menuItem(t('Campus Safety'), 'https://www.hfcc.edu/campus-safety');
        break;
      case 'hfcc_global_life_links':
        $class = 'life-links';
        $items[] = $this->menuItem(t('Student Activities'), 'https://www.hfcc.edu/campus-life/student-activities');
        $items[] = $this->menuItem(t('Student Clubs'), 'https://my.hfcc.edu/students/clubs');
        $items[] = $this->menuItem(t('Student Council'), 'https://www.hfcc.edu/campus-life/student-council');
        $items[] = $this->menuItem(t('Athletics & Intramurals'), 'https://athletics.hfcc.edu/');
        $items[] = $this->menuItem(t('Campus Dining'), 'https://www.hfcc.edu/campus-life/amenities/campus-dining');
        $items[] = $this->menuItem(t('Fifty-One O One'), 'https://5101.hfcc.edu/');
        $items[] = $this->menuItem(t('Wireless Campus'), 'https://my.hfcc.edu/its-help-desk/wireless');
        $items[] = $this->menuItem(t('Mirror News'), 'https://mirrornews.hfcc.edu/');
        $items[] = $this->menuItem(t('Looking Glass'), 'https://glass.hfcc.edu/');
        $items[] = $this->menuItem(t('Theatre'), 'https://www.hfcc.edu/campus-life/theatre');
        break;
      case 'hfcc_global_fatbutton_links':
        $class = 'fatbutton-links';
        $items[] = $this->menuItem(t('HFC Alert'), 'https://www.hfcc.edu/campus-safety/alerts', 'button-alert');
        $items[] = $this->menuItem(t('Facebook'), 'https://www.facebook.com/henryfordcc?sk=wall', 'button-fb');
        $items[] = $this->menuItem(t('Twitter'), 'https://twitter.com/hfcc', 'button-tw');
        $items[] = $this->menuItem(t('Linked In'), 'https://www.linkedin.com/in/henry-ford-college-19737833/', 'button-li');
        $items[] = $this->menuItem(t('RSS'), 'https://feeds.feedburner.com/hfccnews', 'button-rss');
        $items[] = $this->menuItem(t('YouTube'), 'https://www.youtube.com/user/henryfordcc', 'button-yt');
        break;
      case 'hfcc_global_footer_links':
        $class = 'footer-links';
        $items[] = $this->menuItem(t('Campus Map'), 'https://www.hfcc.edu/map');
        $items[] = $this->menuItem(t('Directory'), 'https://www.hfcc.edu/directory');
        $items[] = $this->menuItem(t('Hours of Operation'), 'https://www.hfcc.edu/about-us/hours');
        $items[] = $this->menuItem(t('Privacy Policy'), 'https://www.hfcc.edu/about-us/privacy');
        $items[] = $this->menuItem(t('Terms of Use'), 'https://my.hfcc.edu/aup');
        $items[] = $this->menuItem(t('Request More Information'), 'https://www.hfcc.edu/more-info');
        break;
      case 'hfcc_global_audience_links':
        $class = 'audience-links';
        $items[] = $this->menuItem(t('Students'), 'https://my.hfcc.edu/students');
        $items[] = $this->menuItem(t('Employees'), 'https://my.hfcc.edu/faculty-and-staff');
        $items[] = $this->menuItem(t('Alumni'), 'https://www.hfcc.edu/alumni');
        $items[] = $this->menuItem(t('Community'), 'https://www.hfcc.edu/about/community');
        $items[] = $this->menuItem(t('Give to HFC'), 'https://foundation.hfcc.edu/');
        break;
      default:
        // This should not happen.
        return;
    }
    $output[] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['class' => 'menu ' . $class],
    ];
  }

  /**
   * Return a menu leaf item.
   */
  private function menuItem($title = NULL, $path = NULL, $class = NULL, $tooltip = NULL) {
    $title = t($title);
    $path = check_url($path);
    if ($title && $path) {
      $link_class = !empty($class) ? hfcc_global_id_safe($class) : hfcc_global_id_safe($title);
      $attributes = ['class' => $link_class];

      if (!empty($tooltip)) {
        $attributes['title'] = t($tooltip);
      }

      return [
        'data' => l($title, $path, ['attributes' => $attributes]),
        'class' => ['leaf'],
      ];
    }
    else {
      return [];
    }
  }
}
