<?php

/**
 * Defines President's Banner block.
 */
class HfcGlobalPresidentBannerBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t("HFC Global: President's banner"),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $text = <<<'ENDTEXT'
      <div class="bannertext">
        <h1 class="no-margin"><span class="mock-h2-size">Henry Ford College President</span><br>
        <span class="mock-h1-size">Russell A. Kavalhuna, J.D.</span></h1>
        <p>
          The College's sixth president, Mr. Kavalhuna is leading the way in creating affordable
          access to excellent higher education for all students. His mission is to promote an HFC
          education as a gateway to the middle class, and a gem in the Midwest.
        </p>
      </div>
ENDTEXT;
    $output[] = ['#markup' => $text];
  }
}
