<?php

/**
 * Defines the global site name block.
 */
class HfcGlobalSiteNameBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('HFC Global: Site Name'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $output[] = ['#markup' => l(variable_get('site_name', 'Drupal'), '<front>')];
  }
}
