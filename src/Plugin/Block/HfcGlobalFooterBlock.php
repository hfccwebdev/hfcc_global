<?php

/**
 * Defines the global footer block.
 */
class HfcGlobalFooterBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('HFC Global: Footer message.'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $year = date('Y');
    $output[] = [
      '#theme' => 'item_list',
      '#items' => [
        t('<a href="https://www.hfcc.edu/copyright">Copyright &copy;@year</a> Henry Ford College All rights reserved.', ['@year' => $year]),
        "5101 Evergreen Rd. Dearborn, MI 48128",
        l("admissions@hfcc.edu", "mailto:admissions@hfcc.edu"),
        '800-585-4322',
      ],
      '#type' => 'ul',
      '#attributes' => ['class' => 'menu footer-links'],
    ];
  }
}
