<?php

/**
 * Defines the HFC Global Base Block Interface.
 */
interface HfcGlobalBaseBlockInterface {

  /**
   * Instantiates a new object of this class.
   */
  public static function create();

  /**
   * Returns values for hook_block_info().
   *
   * @return array
   *   - info: the block title.
   *   - cache: caching parameters.
   *
   * @see hook_block_info()
   */
  public function info();

  /**
   * Returns the block label.
   *
   * @return string|NULL
   *   The block label, or nothing.
   */
  public function label();

  /**
   * Returns form for hook_block_configure().
   *
   * @return array|NULL
   *   The block configuration form, or nothing.
   *
   * @see hook_block_configure()
   */
  public function configure();

  /**
   * Saves configuration for hook_block_save().
   *
   * @param array $edit
   *   The configuration form values.
   */
  public function save($edit);

  /**
   * Returns value for hook_block_view().
   *
   * @return array
   *   The block contents.
   *   - subject: the block subject.
   *   - content: the block content render array.
   *
   * @see hook_block_view()
   */
  public function view();

}

/**
 * Defines the HFC Global Base Block.
 */
abstract class HfcGlobalBaseBlock implements HfcGlobalBaseBlockInterface {

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  abstract function info();

  /**
   * {@inheritdoc}
   */
  public function label() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $form = [];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save($edit) {
    // Blocks should process form $edit values here.
  }

  /**
   * {@inheritdoc}
   */
  public function view() {
    $output = [];
    $this->build($output);

    if (!empty($output)) {
      return ['subject' => $this->label(), 'content' => $output];
    }
  }

  /**
   * Build content for this block.
   *
   * @param array $output
   *   The block contents render array.
   */
  abstract protected function build(&$output);
}
