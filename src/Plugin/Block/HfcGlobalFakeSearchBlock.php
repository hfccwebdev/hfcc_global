<?php

/**
 * Defines the global logo block.
 */
class HfcGlobalFakeSearchBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('HFC Global: Fake search button'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $output[] = ['#markup' => t('<button id="search-button-expand">Search</button>')];
  }
}
