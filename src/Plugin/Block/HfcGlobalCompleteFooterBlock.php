<?php

/**
 * Defines the global complete footer block.
 */
class HfcGlobalCompleteFooterBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('HFC Global: Complete Footer.'),
      'cache' => DRUPAL_NO_CACHE, // DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {

    // logo, footer links, hlc, transparency
    // any new private methods you add should also return a renderable array.

    $output[] = [
      '#prefix' => '<div id="complete-footer-logo" class="no-link">',
      $this->logoBlock(),
      '#suffix' => '</div>',
    ];
    $output[] = [
      '#prefix' => '<div id="complete-footer-contact">',
      $this->contactInfo(),
      '#suffix' => '</div>',
    ];
    $output[] = [
      '#prefix' => '<div id="complete-footer-links">',
      $this->footerLinks(),
      '#suffix' => '</div>',
    ];
    $output[] = [
      '#prefix' => '<div id="complete-footer-transparency" class="no-link">',
      $this->transparencyReporting(),
      '#suffix' => '</div>',
    ];
    $output[] = [
      '#prefix' => '<div id="complete-footer-social">',
      $this->socialIcons(),
      '#suffix' => '</div>',
    ];
    $output[] = [
      '#prefix' => '<div id="complete-footer-copyright">',
      $this->copyrightInfo(),
      '#suffix' => '</div>',
    ];
  }

  /**
   * Generate the logo output.
   */
  private function logoBlock() {
    $image = theme('image', [
      'path' => drupal_get_path('module', 'hfcc_global') . '/images/hfc-futuredriven-white.svg',
      'alt' => t('Henry Ford College – Future Driven'),
    ]);

    $link_options = [
      'attributes' => ['class' => 'futuredriven-logo', 'title' => t('Henry Ford College – Future Driven')],
      'html' => TRUE
    ];

    return ['#markup' => l($image, 'https://www.hfcc.edu/', $link_options)];
  }

  /**
   * Generate footer link list.
   */
  private function footerLinks() {

    $items = [

      l(t('HFC Careers'), 'https://www.hfcc.edu/human-resources'),
      l(t('Media Information'), 'https://marcom.hfcc.edu/media'),
      l(t('Campus Safety Information and Resources'), 'https://www.hfcc.edu/campus-safety'),
      l(t('Request Information'), 'https://www.hfcc.edu/more-info'),
      l(t('Accreditation'), 'https://www.hfcc.edu/accreditation'),
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['class' => 'menu'],
    ];
  }

  /**
   * Generate the transparency reporting link.
   */
  private function transparencyReporting() {
    $image = theme('image', [
      'path' => drupal_get_path('module', 'hfcc_global') . '/images/budget-transparency-reporting.png',
      'alt' => t('Budget and Performance Transparency Reporting'),
    ]);

    $link_options = [
      'attributes' => ['class' => 'transp-reporting-logo', 'title' => t('Budget and Performance Transparency Reporting')],
      'html' => TRUE
    ];
    return ['#markup' => l($image, 'https://www.hfcc.edu/about-us/transpreport', $link_options)];
  }

  /**
   * Generate the copyright info.
   */
  private function copyrightInfo() {
    $year = date('Y');

    $items = [
      t('<a href="https://www.hfcc.edu/copyright">Copyright &copy;@year</a> Henry Ford College All rights reserved.', ['@year' => $year]),
      l(t('Privacy Policy'), 'https://www.hfcc.edu/about-us/privacy'),
      l(t('Terms of Use'), 'https://my.hfcc.edu/aup'),
    ];
    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['class' => 'inline'],
    ];
  }

  /**
   * Generate the contact info.
   */
  private function contactInfo() {
    $items = [
      t('Henry Ford College <br>5101 Evergreen Rd.<br>Dearborn, MI 48128'),
      l(t('Campus Map'), 'https://www.hfcc.edu/map', ['attributes' => ['class' => 'contact-border']]),
      l(t('313-845-9600'), 'tel:313-845-9600', ['attributes' => ['class' => 'no-link']]),
      l(t('Contact Us'), 'https://www.hfcc.edu/directory'),
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['class' => 'menu'],
    ];
  }

  /**
   * Generate the social icons.
   */
  private function socialIcons() {
    $items = [
      l(t('Facebook'), 'https://www.facebook.com/henryfordcc', ['attributes' => ['class' => 'no-link social-fb']]),
      l(t('Twitter'), 'https://twitter.com/hfcc', ['attributes' => ['class' => 'no-link social-twitter']]),
      l(t('Linked In'), 'https://www.linkedin.com/in/henry-ford-college-19737833/', ['attributes' => ['class' => 'no-link social-linkedin']]),
      l(t('YouTube'), 'https://www.youtube.com/user/henryfordcc', ['attributes' => ['class' => 'no-link social-youtube']]),
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes' => ['class' => 'menu'],
    ];
  }
}
