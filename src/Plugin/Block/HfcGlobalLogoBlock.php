<?php

/**
 * Defines the global logo block.
 */
class HfcGlobalLogoBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('HFC Global: Logo.'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $image = theme('image', [
      'path' => drupal_get_path('module', 'hfcc_global') . '/images/hfc-futuredriven-white.svg',
      'alt' => t('Henry Ford College – Future Driven'),
      'attributes' => ['class' => 'futuredriven-logo'],
    ]);
    $output[] = ['#markup' => l($image, 'https://www.hfcc.edu/', ['html' => TRUE])];
  }
}
