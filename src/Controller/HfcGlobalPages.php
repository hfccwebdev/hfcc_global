<?php

/**
 * Defines page callbacks.
 */
class HfcGlobalPages {

  /**
   * Page callback to write Base URL to watchdog log.
   *
   * This page was created to help troubleshoot NetIQ Access Manager problems.
   */
  public static function baseUrlPage() {
    global $base_url;
    $message = 'The $base_url is !base_url';
    $values = ['!base_url' => $base_url];
    watchdog('hfcc_global', $message, $values, WATCHDOG_DEBUG);
    return t($message, $values);
  }

  /**
   * Page callback to generate a simple response for New Relic and other
   * availability monitors.
   */
  public static function newrelicPage() {
    drupal_add_http_header('Content-Type', 'text/plain; charset=utf-8');
    if ($last_cron = variable_get('cron_last', NULL)) {
      echo t("cron_last: @i ago\n", ['@i' => time() - $last_cron]);
    }
    else {
      echo t("cron_last: unknown\n");
    }
    // return nothing. We are dumping plain text here.
  }


}
