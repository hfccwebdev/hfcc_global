<?php

/**
 * Contains the HfcGlobalRequirements class.
 */
class HfcGlobalRequirements {

  /**
   * Return content for hook_requirements().
   */
  public static function build($phase) {
    $requirements = [];
    // Ensure translations don't break during installation.
    $t = get_t();

    // Runtime phase shows information on the status report page.
    if ($phase == 'runtime') {

      // STOP TURNING THIS ON!!! WHO IS DOING THIS???
      if (module_exists('php')) {
        $requirements['phpfilter']  = [
          'title' => $t('PHP Text Filter'),
          'value' => $t('Enabling this module is a very bad idea. Turn it off now!!!'),
          'severity' => REQUIREMENT_ERROR,
          'weight' => -99,
        ];
      }

      // Shows current platform directory. This might seem like a
      // security risk, but anyone who can view this can also read
      // the php_info() report, so how is that any different?
      $hostname = phpversion() >= '5.3.0' ? gethostname() : php_uname('n');
      $requirements['platform'] = [
        'title' => $t('Platform'),
        'value' => $t('@hostname:@dir', ['@hostname' =>$hostname, '@dir' => getcwd()]),
        'severity' => REQUIREMENT_INFO,
        'weight' => -8,
      ];
      if (isset($_SERVER['HTTP_VIA']) || isset($_SERVER['HTTP_FORWARDED']) || isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $rows = [];
        foreach ([
          'HTTP_CONNECT',
          'HTTP_FORWARDED',
          'HTTP_VIA',
          'HTTP_X_FORWARDED_FOR',
          'HTTP_X_FORWARDED_HOST',
          'HTTP_X_FORWARDED_SERVER',
        ] as $key) {
          if (isset($_SERVER[$key])) {
            $rows[] = t('@key: @value', ['@key' => $key, '@value' => $_SERVER[$key]]);
          }
        }
        $requirements['proxies'] = [
          'title' => $t('Proxy Servers'),
          'value' => implode('<br>', $rows),
          'severity' => REQUIREMENT_INFO,
          'weight' => -8,
        ];
      }
    }
    $requirements['realpath_cache_size'] = [
      'title' => $t('Realpath Cache Size'),
      'value' => $t('@size', ['@size' => realpath_cache_size()]),
      'severity' => REQUIREMENT_INFO,
      'weight' => -7,
    ];
    return $requirements;
  }
}
