<?php

/**
 * Defines the HFC Global Utilities service interface.
 */
interface HfcGlobalUtilsInterface {

  /**
   * Creates an instance of this service.
   */
  public function create();

  /**
   * Sanitize and format phone numbers.
   *
   * @param string $value
   *   The phone number to format.
   * @param bool $allow_extension
   *   Allow displaying 4-digit internal extensions.
   *
   * @return string
   *   The formatted phone number.
   */
  public static function formatOfficePhone($value, $allow_extension = FALSE);

  /**
   * Check for orphaned field instances.
   *
   * @param bool $remove
   *   Flag to automatically remove orphaned instances.
   *
   * @see field_ui_fields_list()
   */
  public function findOrphanedFields($remove = FALSE);

}
